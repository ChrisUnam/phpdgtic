<?php 
	include_once 'form.php';
	$cuenta=$_POST['cuenta'];
	$nombre=$_POST['nombre'];
	$apellidoPaterno=$_POST['apellidoPaterno'];
	$apellidoMaterno=$_POST['apellidoMaterno'];
	$genero=$_POST['genero'];
	$fechaNacimiento=$_POST['fechaNacimiento'];

	try {
		$sql = "INSERT INTO alumno(al_numcta, al_nombre, al_apellido1, al_apellido2, al_genero, al_fechaNac) VALUES(?,?,?,?,?,?)";

		$stmt = $dbh->prepare($sql);
		$stmt->bindParam(1,$cuenta);
		$stmt->bindParam(2,$nombre);
		$stmt->bindParam(3,$apellidoPaterno);
		$stmt->bindParam(4,$apellidoMaterno);
		$stmt->bindParam(5,$genero);
		$stmt->bindParam(6,$fechaNacimiento);

		echo ($stmt->execute()) ? "se agrego registro" : "no se agrego registro";

	} catch (Exception $e) {
		echo "error con la base";
	}
	

 ?>

 <!DOCTYPE html>
 <html>
 <head>
 	<title>Registro exitoso!</title>
 </head>
 <body>
 	<h1>Se ha agregado el registro</h1>
 	<a href="tabla.php">Ver registros</a>
 	<a href="formulario.php">Regresar</a>
 </body>
 </html>