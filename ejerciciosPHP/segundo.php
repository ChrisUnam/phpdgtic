<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="uno.css">
	<title>Ejercicio php 2</title>
</head>
<body>
	<div class="contenedor">
		<div class="hijo">
			<?php echo "Este es un ejemplo de substr('abcdef', 1, 3): ";
				echo substr('abcdef', 1, 3);
				echo "<br>";
				echo "Este es un ejemplo de strstr. <br>";
				echo "Correo: chris@gmail.com";
				$correo='chris@gmail.com';
				echo "Domain = strstr(email,'@'): ";
				echo strstr($correo, '@').'<br>';

				echo "Nombre = strstr(correo,'@',true): ";
				echo strstr($correo,'@',true);
				echo "<br>Este es un ejemplo strpos el cual busca una cadena en otra y regresa la posicion (primer caracter)".'<br>';
				echo "Mi cadena: Hola soy alberto. <br>";
				$micadena = 'Hola soy alberto';
				echo "Cadena a encontrar: alberto. <br>";
				$encontrar = 'alberto';
				echo "Se procede a buscar albert y alberto. <br>";
				$res1=strpos($micadena,$encontrar);
				$res2=strpos($micadena,'abert');
				if ($res1 === false) {
				    echo "La cadena '$encontrar' no fue encontrada en la cadena '$micadena'";
				} else {
				    echo "La cadena '$encontrar' fue encontrada en la cadena '$micadena'";
				    echo " y existe en la posición $res1";
				}
				echo "<br>";
				if ($res2 === false) {
				    echo "La cadena 'albert' no fue encontrada en la cadena '$micadena'";
				} else {
				    echo "La cadena '$encontrar' fue encontrada en la cadena '$micadena'";
				    echo " y existe en la posición $res1";
				}

				echo "<br>implode — Une elementos de un array en un string<br>";
				$array = array('apellido', 'email', 'teléfono');
				$separado_por_comas = implode(",", $array);

				echo $separado_por_comas; 
				echo "<br>explode — Divide un string en varios string con base a un delimitador<br>";
				$pizza  = "porción1 porción2 porción3 porción4 porción5 porción6";
				$porciones = explode(" ", $pizza);
				echo $porciones[0].'<br>'; // porción1
				echo $porciones[1]; // porción2
				echo "Array_pop<br>";
				echo "Extrae el último elemento del final del array<br>";
				echo "Siguiendo el ejemplo de la pizza tenemos una con pina jamon y peperoni:<br>";
				$pizza = array("pina","jamon","peperoni");
				print_r($pizza);
				echo array_pop($pizza).'<br>';
				print_r($pizza);
				echo "<br>Array_push<br>";
				echo "Inserta uno o más elementos al final de un array,vamos a agregar un complemento<br>";
				echo array_push($pizza,"harina");
				print_r($pizza);
				echo "array_diff(array1, array2)";
				echo "<br>Calcula la diferencia entre arrays<br>";
				$array1 = array("verde","azul","rojo");
				$array2 = array("verde","rojo");
				$resultado = array_diff($array1,$array2);
				print_r($resultado);
				echo "array_walk(array, funcname)<br>";
				echo "Aplicar una función proporcionada por el usuario a cada miembro de un array<br>";
				$sumar = function($suma){
					echo 5+$suma."<br>";
				};
				$serie = array(10,5,10,5,15);
				print_r($serie);
				array_walk($serie,$sumar);
				echo "Sort<br>";
				echo "Ordena un array<br>";
				sort($pizza);
				print_r($pizza);
				echo "Current<br>";
				echo "Devuelve el elemento actual en un array<br>";
				echo current($pizza).'<br>';
				echo next($pizza).'<br>';
				echo "DATE<br>";
				echo "Dar formato a la fecha/hora local<br>";
				date_default_timezone_set('UTC');
				echo date("l").'<br>';
				echo "Empty<br>";
				echo "Determina si una variable está vacía<br>";
				$var = 0;

				// Se evalúa a true ya que $var está vacia
				if (empty($var)) {
				    echo '$var es o bien 0, vacía, o no se encuentra definida en absoluto<bar>';
				}

				$var = 15;
				// Se evalúa como true ya que $var está definida
				if (isset($var)) {
				    echo '$var está definida a pesar que está vacía';
				}



			 ?>

		</div>
	</div>
</body>
</html>