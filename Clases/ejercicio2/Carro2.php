<?php
//creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;
	private $anio;

	//declaracion del método verificación
	public function verificacion(){
		if ($this->anio>"2010-01") {
			echo "Sí circula";
		}elseif ($this->anio<"1990-01") {
			echo "No circula";
		}else{
			echo "Revision";
		}
	}

	public function setAnio($valor)
	{
		$this->anio=$valor;
	}

	public function getAnio()
	{
		return $this->anio;
	}
}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();

if (!empty($_POST)){
	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];
	$Carro1->setAnio($_POST['anio']);
}




