<?php 
	include_once 'transporte.php';

	/**
	 * 
	 */
	class Bici extends transporte
	{
		private $tipo;
		function __construct($nom, $vel, $com, $tipo)
		{
			parent::__construct($nom,$vel,$com);
			$this->tipo = $tipo;
		}
		public function resumenBici(){
			$mensaje=parent::crear_ficha();
			$mensaje.='<tr>
						<td>Tipo:</td>
						<td>'. $this->tipo.'</td>				
					</tr>';
			return $mensaje;
		}
	}

 ?>