<?php 
include_once('Carro.php');
include_once('Moto.php');
include_once('Avion.php');
include_once('bici.php');


$mensaje='';


if (!empty($_POST)){
	//declaracion de un operador switch
	switch ($_POST['tipo_transporte']) {
		case 'aereo':
			//creacion del objeto con sus respectivos parametros para el constructor
			$jet1= new avion('jet','400','gasoleo','2');
			$mensaje=$jet1->resumenAvion();
			break;
		case 'terrestre':
			$carro1= new carro('carro','200','gasolina','4');
			$mensaje=$carro1->resumenCarro();
			break;
		case 'maritimo':
			$bergantin1= new barco('bergantin','40','na','15');
			$mensaje=$bergantin1->resumenBarco();
			break;	
		case 'bicicleta':
			$bici = new Bici('cannodale','60','humano','ruta');
			$mensaje = $bici->resumenBici();
	}

}

 ?>